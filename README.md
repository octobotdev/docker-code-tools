# docker-code-tools
Docker image with installed utilities for code analysis and linter.

Currently, it provides the following:
- Python flake8
- JavaScript eslint

It provides a volume to mount your code and run the different tools. Both offer
configuration through files, which eases configuration options.

Example usage:
- `docker run --rm -it -v mypythonproject:/var/code/ flake8`
- `docker run --rm -it -v myjsproject:/var/code/ eslint`
